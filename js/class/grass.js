'use strict';

const CSprite = require('./sprite');

class CGrass extends CSprite
{
    move()
    {
        this.sprite.x -= 0.5;

        if (this.sprite.x < -70) {
            return false;
        }
    }
}

module.exports = CGrass;