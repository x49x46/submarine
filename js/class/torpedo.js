'use strict';

const Intersects = require('intersects');
const {Howl} = require('howler');
const CBoom = require('./boom');
const CSprite = require('./sprite');
const CBubble = require('./bubble');
const CConfig = require('./config');

class CTorpedo extends CSprite
{
    constructor(x, y, image, progress)
    {
        super(x, y, image);

        this.sprite.width = 80;
        this.sprite.height = 15;
        this.sprite.zIndex = 25;

        this.progress = progress;
        this.submarine = progress.submarine;

        this.x_pos = x;
        this.y_pos = y;

        this.sound = new Howl({
            src: ['./sounds/mine2.mp3'],
            volume: 0.3
        });

        this.soundLaunch = new Howl({
            src: ['./sounds/torpedo.mp3'],
            volume: 0.5
        });

        this.soundLaunchIsPlaying = false;
    }

    move(interval)
    {
        this.sprite.x -= this.progress.global_speed * 4;

        if (this.sprite.x < 1280 && this.soundLaunchIsPlaying === false) {
            this.soundLaunch.play();
            this.soundLaunchIsPlaying = true;
        }

        if (Intersects.circlePolygon(
            this.sprite.x + Math.round(this.sprite.width / 2),
            this.sprite.y + Math.round(this.sprite.height / 2),
            Math.round(this.sprite.height / 2),
            this.submarine.getPolygon())) {

            let boom = new CBoom(this.sprite.x, this.sprite.y, this.progress.global_speed);

            this.progress.life -= 1;
            this.sound.play();

            if (this.progress.life < 3) {
                this.progress.submarine.damage(this.progress.life);
            }

            this.progress.global_speed = this.progress.global_speed * 0.75;

            this.progress.stage.append(boom);

            return false;
        }

        if (this.x < -this.sprite.width) {
            return false;
        }

        let _step = Math.ceil(10 / this.progress.global_speed);
        if (_step > 10) {
            _step = 10;
        }

        if (interval % _step == 0) {
            let bubble = new CBubble(this.sprite.x + this.sprite.width, this.sprite.y + this.sprite.height / 2, './images/bubble.png', this.progress);
            this.progress.stage.append(bubble);
        }
    }
}

module.exports = CTorpedo;