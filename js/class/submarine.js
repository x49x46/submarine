'use strict';

const $ = require('jquery');
const CConfig = require('./config');
const CSprite = require('./sprite');
const CBubble = require('./bubble');
const CBigBoom = require('./bigboom');

class CSubmarine extends  CSprite
{


    constructor(x, y, progress) {

        super(x, y, '');
        this.progress = progress;
        let self = this;
        this.config = new CConfig();
        this.zOrder = 10;
        this.die = false;
        this.polygon = [
            0,64,
            0, 18,
            15, 18,
            15, 37,
            29, 34,
            45, 30,
            60, 28,
            60, 23,
            68, 23,
            76, 18,
            90, 13,
            90, 5,
            96, 0,
            96, 14,
            102, 10,
            102, 14,
            108, 18,
            112, 24,
            112, 29,
            156, 27,
            159, 31,
            159, 49,
            156, 55,
            146, 64
        ];

        this.speed_y = 0.5;
        this.speed_x = 0.5;
        this.vector_y = 1;

        this.soundEngine = new Howl({
            src: ['./sounds/engine2.mp3'],
            volume: 0.15,
            loop: true,
        });

        this.upListgener = () => {
            self.vector_y = -1;
            this.soundEngine.play();
        };

        this.downListener = () => {
            self.vector_y = 1;
            this.soundEngine.stop();
        };

        this.addListeners();

        let frames = [
            './images/submarine_1.png',
            './images/submarine_2.png',
            './images/submarine_3.png'
        ];

        let textureArray = [];

        for (let i = 0; i < frames.length; i++)
        {
            let texture = PIXI.loader.resources[frames[i]].texture; //PIXI.Texture.from(frames[i]);
            textureArray.push(texture);
        }

        this.sprite = new PIXI.extras.AnimatedSprite(textureArray); //new PIXI.extras.AnimatedSprite.fromImages(frames);
        this.damage(3);
        this.sprite.width = 160;
        this.sprite.height = 65;
        this.sprite.animationSpeed = 0.2;
        this.sprite.play();
        this.sprite.x = x;
        this.sprite.y = y;
        this.sprite.zIndex = 100;

        this.sound = new Howl({
            src: ['./sounds/mine2.mp3'],
            volume: 0.2
        });
    }

    damage(value)
    {

        if (!value) {
            return false;
        }

        this.damage1 = [
            './images/ddamage1.png',
            './images/ddamage2.png',
            './images/ddamage3.png'
        ];

        this.damage2 = [
            './images/damage1.png',
            './images/damage2.png',
            './images/damage3.png'
        ];

        this.damage3 = [
            './images/submarine_1.png',
            './images/submarine_2.png',
            './images/submarine_3.png'
        ];

        let frames = [];

        if (value == 1) {
            frames = this.damage1;
        }

        if (value == 2) {
            frames = this.damage2;
        }

        if (value >= 3) {
            frames = this.damage3;
        }

        let textureArray = [];

        for (let i = 0; i < frames.length; i++)
        {
            let texture = PIXI.loader.resources[frames[i]].texture; //PIXI.Texture.from(frames[i]);
            textureArray.push(texture);
        }

        this.sprite._textures = textureArray;
    }

    getPolygon()
    {
        let polygon_position = [];

        for (let i = 0; i < this.polygon.length; i++) {
            if (i % 2 === 0) {
                polygon_position[i] = this.polygon[i] + this.sprite.x;
            } else {
                polygon_position[i] = this.polygon[i] + this.sprite.y;
            }
        }

        return polygon_position;
    }

    move(interval)
    {
        let _step = Math.ceil(10 / this.progress.global_speed);
        if (_step > 10) {
            _step = 10;
        }

        if (interval % _step == 0) {
            let bubble = new CBubble(this.sprite.x, this.sprite.y + this.sprite.height / 2 + 6, './images/bubble.png', this.progress);
            this.progress.stage.append(bubble);

            //зависимость
            if (this.speed_y < 2) {
                this.speed_y = this.progress.global_speed / 3;

                if (this.speed_y < 0.5) {
                    this.speed_y = 0.5;
                }
            }
        }

        this.sprite.y += this.speed_y * this.vector_y;

        if (this.sprite.y < 0) {
            this.sprite.y = 0;
        }

        if (this.sprite.y > this.config.screen.height - this.sprite.height - 20 || this.die) {
            $('#wnd-game-over').css('display', 'inline-block');
            $('#win-line').css('display', 'inline-block');
            $('#restart').removeClass('hidden');
            this.polygon = [];

            let boom = new CBigBoom(this.sprite.x, this.sprite.y, '');

            this.progress.stage.append(boom);

            if (this.sprite.x > 0) {
                this.progress.send('end');
            }

            if (!this.progress.game_over) {
                this.sound.play();
            }

            this.removeListeners();
            //this.progress.music.stop();
            this.progress.game_over = true;
            clearInterval(this.progress.time_interval);

            return false;
        }
    }

    addListeners() {
        this.removeListeners();
        window.onmousedown = this.upListgener;
        window.onmouseup = this.downListener;
        window.ontouchstart = this.upListgener;
        window.ontouchend = this.downListener;
    }

    removeListeners() {
        window.onmousedown = null;
        window.onmouseup = null;
        window.ontouchstart = null;
        window.ontouchend = null;
    }

    boom()
    {
        this.die = true;
    }
}

module.exports = CSubmarine;