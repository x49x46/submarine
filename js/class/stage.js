'use strict';

class CStage {
    constructor(app) {

        this.app = app;

        this.active = true;
        this.objects = [];
    }

    append(sprite) {
        this.objects.push(sprite);
        this.app.stage.addChild(sprite.sprite);
        this.sortZ();
    }

    disabled() {
        this.active = false;

        //TODO: выбор между сокрытием и уничтожением
        this.objects.forEach(function(item, i, arr) {
            //item.sprite.visible = false;
            this.app.stage.removeChild(item.sprite);
        });
    }

    enabled() {
        this.active = true;

        this.objects.forEach(function(item, i, arr) {
            //item.sprite.visible = true;
        });
    }

    move(delta) {
        if (this.active) {
            //TODO: performance
            if (delta % 50 == 0) {

            }

            let self = this;
            this.objects.forEach(function(item, i, arr) {
                if (item.move(delta) === false) {
                    self.app.stage.removeChild(self.objects[i].sprite);
                    delete self.objects[i];
                }
            });
        }
    }

    sortZ()
    {
        this.app.stage.children.sort(function(a, b){
            a.zIndex = a.zIndex ? a.zIndex : 0;
            b.zIndex = b.zIndex ? b.zIndex : 0;

            if(a.zIndex === b.zIndex) return 0;
            else return (a.zIndex < b.zIndex ? -1 : 1);
        });
    }

    clear() {
        let self = this;
        this.objects.forEach(function(item, i, arr) {
            self.app.stage.removeChild(self.objects[i].sprite);
            delete self.objects[i];
        });
    }
}

module.exports = CStage;

