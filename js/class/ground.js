'use strict';

const CSprite = require('./sprite');
const CConfig = require('./config');

class CGround extends CSprite
{
    constructor(x, y, image, progress)
    {
        super(x, y, image);
        this.progress = progress;
        this.stage = progress.stage;
        this.recreate = false;
        this.sprite.width = 1280;
        this.sprite.height = 106;
        this.sprite.zIndex = 5;
        this.config = new CConfig();
        this.bigfig = 'VnJeKxvrRH';
    }

    move()
    {
        this.sprite.x -= this.progress.global_speed * 1.2;

        if (this.sprite.x < 0 && !this.recreate) {
            let dno = new CGround(this.sprite.x + 1280, this.config.screen.height - 106, './images/ground.png', this.progress);
            this.stage.append(dno);
            this.recreate = true;
        }

        if (this.sprite.x < -1280) {
            return false;
        }
    }
}

module.exports = CGround;