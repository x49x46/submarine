'use strict';

const CSprite = require('./sprite');

const Intersects = require('intersects');

class CHeart extends CSprite
{
    constructor(x, y, image, progress)
    {
        super(x, y, image);

        this.sprite.width = 30;
        this.sprite.height = 30;
        this.sprite.zIndex = 5;

        this.progress = progress;
        this.submarine = progress.submarine;

        this.amplitude = 7;
        this.sin_speed = 10;
        this.x_pos = x;
        this.y_pos = y;

        this.sound = new Howl({
            src: ['./sounds/heart.mp3'],
            volume: 0.3
        });
    }

    move(interval)
    {
        this.x_pos -= this.progress.global_speed;

        this.sprite.y  = Math.sin(interval / this.sin_speed) * this.amplitude + this.y_pos;
        this.sprite.x = Math.cos(interval / this.sin_speed) * this.amplitude + this.x_pos;

        if (Intersects.boxPolygon(
            this.sprite.x,
            this.sprite.y,
            this.sprite.widows,
            this.sprite.height,
            this.submarine.getPolygon())) {

            this.progress.life += 1;

            if (this.progress.life <= 3) {
                this.progress.submarine.damage(this.progress.life);
            }

            this.sound.play();

            return false;
        }

        if (this.x < -this.sprite.width) {
            return false;
        }
    }
}

module.exports = CHeart;