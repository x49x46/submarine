'use strict';

const $ = require('jquery');
const {Howl} = require('howler');
const CConfig = require('./config');
const CDno = require('./dno');
const CMine = require('./mine');
const CCoin = require('./coin');
const CHeart = require('./heart');
const CSubmarine = require('./submarine');
const CBubble = require('./bubble');
const CBackground = require('./background');
const CGround = require('./ground');
const CFishes = require('./fishes');
const CTorpedo = require('./torpedo');
const CAhtung = require('./ahtung');
const CryptoJS = require('crypto-js');

class CProgress
{
    constructor(stage)
    {
        this.salat = 'mbPX8XRnsI';
        this.stage = stage;
        this.config = new CConfig();
        this.score_el = $('#score');
        this.timer_el = $('#time');
        this.time = 0;
        this.old_life = 3;

        this.send_end = true;

        this.init();
    }

    initSub()
    {
        this.game_over = false;
        this.submarine = new CSubmarine(200, 250, this);
        this.stage.append(this.submarine);
        this.send('start');
        //this.music.play();
    }

    initSubSub()
    {
        this.game_over = true;
        this.life = 0;
        this.submarine = new CSubmarine(-400, 200, this);
        this.stage.append(this.submarine);
        this.submarine.polygon = [];
    }

    init() {
        this.game_over = false;

        this.old_life = 3;
        this.life = 3;
        this.progress = 0;
        this.progress_heart = 4000;
        this.progress_mine = 1;
        this.progress_torpedo = 1000;
        this.progress_torpedo_count = 1;
        this.progress_coin = 1;

        this.global_speed = 2;

        this.score = 0;

        //bgbg
        for (let i = 0; i < 2; i++) {
            let bg = new CBackground(i * 1280, this.config.screen.height - 450, './images/bgbg.png', this);
            this.stage.append(bg);
        }

        //bg
        for (let i = 0; i < 2; i++) {
            let dno = new CDno(i * 1280, this.config.screen.height - 383, './images/bg.png', this);
            this.stage.append(dno);
        }

        //Дно
        for (let i = 0; i < 2; i++) {
            let dno = new CGround(i * 1280, this.config.screen.height - 106, './images/ground.png', this);
            this.stage.append(dno);
        }

        // if (!this.music) {
        //     // this.music = new Howl({
        //     //     src: ['./sounds/music2.mp3'],
        //     //     loop: true,
        //     //     volume: 0.05
        //     // });
        // }



        // if (this.music) {
        //     console.log('stop music');
        //     this.music.stop();
        // }

        this.time = 0;

        this.showLives();

        let pad = "00";
        let self = this;

        this.time_interval = setInterval(function() {

            //time
            self.time++;
            let minutes = Math.floor(self.time / 60);
            let seconds = self.time - minutes * 60;

            let pad_minutes = pad.substring(0, pad.length - minutes.toString().length) + minutes;
            let pad_seconds = pad.substring(0, pad.length - seconds.toString().length) + seconds;

            self.timer_el.html(pad_minutes + ':' + pad_seconds);
        }, 1000);
    }

    showLives()
    {
        let tpl_life = '<img src="./images/heart.png" width="12" height="12" alt="">';
        let lives = '';

        for (let i = 0; i < this.life; i++) {
            lives += tpl_life;
        }

        $('#life').html(lives);
        this.old_life = this.life;
    }

    send(cmd)
    {
        let coin = new CCoin('gold', this);
        let ground = new CGround(0,0, './images/ground.png', this);
        let data = {
            score: this.score,
            time: this.time,
            command: cmd
        };

        data.hash = CryptoJS.MD5(this.salat + JSON.stringify(data) + coin.flip + ground.bigfig).toString();
        $.post("/misc/submarine/submarine.php", data, function(data) {
            if (cmd == 'end') {
                if (data.success) {

                    $('#result-list').html('');

                    let uid = '';

                    if (data.data.user) {
                        uid = data.data.user.idClient;
                    }

                    let bold_class = '';
                    let you = false;

                    $.each(data.data.top, function(index, row) {

                        bold_class = row.idClient == uid ? 'bolder' : '';
                        if (!you) {
                            you = row.idClient == uid;
                        }

                        $('#result-list').append('<li class="' + bold_class + '"><span>' + row.name + '</span> <span class="right">' + row.scores + '</span></li>');
                    });

                    if (!you && uid) {
                        $('#result-list').append('<li class="' + bold_class + '"><span>' + data.data.user.name + '</span> <span class="right">' + data.data.user.scores + '</span></li>');
                    }

                    if (uid && data.data.user.discount) {
                        $('#discount .discount').html(data.data.user.discount + '%');
                        $('#discount').css('display', 'block');
                    }

                    $('#result').css('display', 'block');
                    $('#rules').css('display', 'none');
                    $('#rulesButton').removeClass('hidden');
                }
            }
        });
    }

    move()
    {
        if (!this.game_over) {
            this.progress++;
        }

        if (this.life != this.old_life) {
            this.showLives();
        }

        this.score_el.html(this.score);

        if (this.life <= 0) {
            $('#wnd-game-over').css('display', 'inline-block');
            $('#win-line').css('display', 'inline-block');

            $('#restart').removeClass('hidden');
            this.game_over = true;
            this.submarine.boom();
            //this.music.stop();
            this.submarine.soundEngine.stop();
            clearInterval(this.time_interval);

            if (!this.send_end && this.time > 0) {
                this.send('end');
            }
        }

        //step
        if (this.progress % 100 == 0) {
            let count = 0;

            if (!this.game_over) {
                //Пузыри
                let bubble = new CBubble(this.config.screen.width * Math.random(), this.config.screen.height, './images/bubble.png', this);
                this.stage.append(bubble);

                //Старая торпеда
                if (this.progress % this.progress_torpedo == 0) {
                    this.progress_torpedo = 1000 * Math.round(Math.random() * 4);

                    if (this.progress_torpedo_count < 7) {
                        this.progress_torpedo_count += 1;
                    }

                    count = Math.round(Math.random() * this.progress_torpedo_count);
                    for (let i = 0; i < count; i++) {
                        let y = Math.random() * (this.config.screen.height - 100) + 60;

                        let torpedo = new CTorpedo(800 + this.config.backPosition(80), y, './images/torpedo.png', this);
                        this.stage.append(torpedo);

                        let ahtung = new CAhtung(this.config.screen.width - 100, y - 50, './images/ahtung.png');
                        this.stage.append(ahtung);
                    }
                }

                //Мины
                count = Math.round(Math.random() * this.progress_mine * (this.global_speed / 2));

                for (let i = 0; i < count; i++) {
                    let mine = new CMine(this.config.backPosition(40), Math.random() * this.config.screen.height - 60, './images/mine.png', this);
                    this.stage.append(mine);
                }

                //Монеты
                count = Math.round(Math.random() * this.progress_coin);

                for (let i = 0; i < count; i++) {

                    let type = Math.round(Math.random());

                    if (type == 0) {
                        type = 'silver';
                    } else {
                        type = 'gold';
                    }

                    let coin = new CCoin(type, this);
                    this.stage.append(coin);
                }

                // if (this.progress_grass % 1500 == 0) {
                //     this.progress_grass = Math.round(Math.random() * 2);
                // }

                if (this.progress % 2000 == 0) {
                    this.progress_coin++;
                }

                if (this.progress % 3000 == 0) {
                    this.progress_mine++;
                }

                if (this.progress % 500 == 0) {
                    this.global_speed += 0.2;

                    if (this.life > 0 && this.submarine.polygon.length > 0) {
                        this.send('ping');
                    }
                }

                if (this.progress % this.progress_heart == 0) {
                    this.progress_heart = 1000 * Math.round(Math.random() * 10);

                    let heart = new CHeart(this.config.backPosition(40), Math.random() * this.config.screen.height - 100, './images/heart.png', this);
                    this.stage.append(heart);
                }

                if (this.progress % 1000 == 0) {
                    //Рыбы
                    let fishes = new CFishes(this.config.backPosition(40), Math.random() * 250, './images/fish3.png', this);
                    this.stage.append(fishes);
                }
            }
        }
    }
}

module.exports = CProgress;