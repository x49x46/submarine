'use strict';

class CPreloader
{
    constructor()
    {
        this.counter = 0;
        this.path = './images/';
        this.images = [
            'bg.png',
            'bgbg.png',
            'bigboom1.png',
            'bigboom2.png',
            'bigboom3.png',
            'bigboom4.png',
            'bigboom5.png',
            'boom1.png',
            'boom2.png',
            'boom3.png',
            'boom4.png',
            'boom5.png',
            'bubble.png',
            'coin_gold.png',
            'coin_silver.png',
            'heart.png',
            'mine.png',
            'submarine_1.png',
            'submarine_2.png',
            'submarine_3.png'
        ];
    }

    load()
    {
        let self = this;

        return new Promise((resolve) => {
            // выполняется асинхронная операция, которая в итоге вызовет:

            for (let i = 0; i < this.images.length; i++) {
                let img = new Image();
                img.src = this.path + this.images[i];
                img.onload = function(event) {
                    self.counter++;

                    if (self.isComplete()) {
                        resolve(true);
                    }
                }
            }
        });
    }

    isComplete()
    {
        return this.counter == this.images.length;
    }
}

module.exports = CPreloader;