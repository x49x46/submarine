'use strict';

const Intersects = require('intersects');
const {Howl} = require('howler');
const CBoom = require('./boom');
const CSprite = require('./sprite');
const CConfig = require('./config');

class CMine extends CSprite
{
    constructor(x, y, image, progress) {
        super(x, y, image);

        this.config = new CConfig();
        this.submarine = progress.submarine;
        this.progress = progress;
        this.max_amplitude = 10;
        this.max_sin_delimiter = 40;
        this.min_sin_delimiter = 20;

        this.amplitude = Math.random() * this.max_amplitude;
        this.sin_speed = Math.random() * this.max_sin_delimiter;

        if (this.sin_speed < this.min_sin_delimiter) {
            this.sin_speed = this.min_sin_delimiter;
        }

        this.sprite.width = 40;
        this.sprite.height = 45;
        this.sprite.zIndex = 10;

        this.y_pos = Math.random() * this.config.screen.height - this.sprite.height;
        this.forsage = 1 + Math.random() / 2;

        if (this.config.screen.height - this.y_pos < 200) {
            this.forsage = 1.1;
        }

        if (this.config.screen.height - this.y_pos < 100) {
            this.forsage = 1.2;
        }

        this.sound = new Howl({
            src: ['./sounds/mine2.mp3'],
            volume: 0.2
        });
    }

    move(interval)
    {
        this.sprite.x -= this.progress.global_speed * this.forsage;

        let y = Math.sin(interval / this.sin_speed) * this.amplitude + this.y_pos;

        if (y > this.config.screen.height - this.sprite.height) {
            y = this.config.screen.height - this.sprite.height;
        }

        this.sprite.y = y;

        if (Intersects.circlePolygon(
            this.sprite.x + Math.round(this.sprite.width / 2),
            this.sprite.y + Math.round(this.sprite.height / 2),
            Math.round(this.sprite.height / 2),
            this.submarine.getPolygon())) {

            let boom = new CBoom(this.sprite.x, this.sprite.y, this.progress.global_speed);

            this.progress.life -= 1;
            this.sound.play();

            if (this.progress.life < 3) {
                this.progress.submarine.damage(this.progress.life);
            }

            this.progress.global_speed = this.progress.global_speed * 0.75;

            this.progress.stage.append(boom);

            return false;
        }

        if (this.x < -this.sprite.width) {
            return false;
        }
    }
}

module.exports = CMine;