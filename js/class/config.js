'use strict';

class CConfig
{
    constructor()
    {
        this.screen = {
            width: 1280,
            height: 720
        };
    }

    backPosition(width = 0)
    {
        return this.screen.width + width + Math.random() * 1280;
    }
}

module.exports = CConfig;