'use strict';

const CSprite = require('./sprite');

class CAhtung extends CSprite
{
    constructor(x, y, image)
    {

        super(x, y, image);

        let self = this;

        this.sprite.width = 83;
        this.sprite.height = 100;
        this.sprite.zIndex = 300;
        this.interval = 0;

        this.sprite.onComplete = function() {
            self.complete = true;
        };

        this.sound = new Howl({
            src: ['./sounds/alert.mp3'],
            volume: 0.1
        });

        this.sound.play();
    }

    move(interval)
    {
        this.sprite.alpha = Math.sin(this.interval / 10);

        this.interval++;

        if (this.interval > 120) {
            this.sound.stop();
            return false;
        }
    }
}

module.exports = CAhtung;