'use strict';

const CSprite = require('./sprite');

class CBoom extends CSprite
{
    constructor(x, y, speed)
    {
        let frames = [
            './images/boom1.png',
            './images/boom2.png',
            './images/boom3.png',
            './images/boom4.png',
            './images/boom5.png'
        ];

        super(x, y, frames[0]);

        let self = this;

        this.speed = speed;
        this.complete = false;

        let textureArray = [];

        for (let i = 0; i < frames.length; i++)
        {
            let texture = PIXI.loader.resources[frames[i]].texture; // PIXI.Texture.from(frames[i]);
            textureArray.push(texture);
        }

        this.sprite = new PIXI.extras.AnimatedSprite(textureArray); //new PIXI.extras.AnimatedSprite.fromImages(frames);
        this.sprite.width = 36;
        this.sprite.height = 36;
        this.sprite.animationSpeed = 0.2;
        this.sprite.loop = false;
        this.sprite.play();
        this.sprite.x = x;
        this.sprite.y = y;
        this.sprite.zIndex = 200;

        this.sprite.onComplete = function() {
            self.complete = true;
        }
    }

    move()
    {
        this.sprite.x -= this.speed;

        return !this.complete;
    }
}

module.exports = CBoom;