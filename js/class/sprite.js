'use strict';

const PIXI = require('pixi.js');

/**
 * Базовый класс объекта на экране
 */
class CSprite {
    constructor(x = 0, y = 0, image = 'submarine.png') {
        this.image = image;

        if (image) {
            this.sprite = new PIXI.Sprite(PIXI.loader.resources[image].texture);
        }

        //PIXI.Sprite.fromImage(this.image);
        //PIXI.loader.resources[image].texture

        if (this.sprite) {
            this.sprite.x = parseFloat(x);
            this.sprite.y = parseFloat(y);

            this.sprite.zIndex = 1;
        }
    }

    get x() {
        return this.sprite.x;
    }

    set x(x) {
        this.sprite.x = parseFloat(x);
    }

    get y() {
        return this.sprite.y;
    }

    set y(y) {
        this.sprite.y = parseFloat(y);
    }

    move(delta) {

    }
}

module.exports = CSprite;