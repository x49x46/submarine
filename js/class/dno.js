'use strict';

const CSprite = require('./sprite');
const CConfig = require('./config');

class CDno extends CSprite
{
    constructor(x, y, image, progress)
    {
        super(x, y, image);
        this.progress = progress;
        this.stage = progress.stage;
        this.recreate = false;
        this.sprite.width = 1280;
        this.sprite.height = 383;
        this.sprite.zIndex = 3;
        this.config = new CConfig();
    }

    move()
    {
        this.sprite.x -= this.progress.global_speed;

        if (this.sprite.x < 0 && !this.recreate) {
            let dno = new CDno(this.sprite.x + 1280, this.config.screen.height - 383, './images/bg.png', this.progress);
            this.stage.append(dno);
            this.recreate = true;
        }

        if (this.sprite.x < -1920) {
            return false;
        }
    }
}

module.exports = CDno;