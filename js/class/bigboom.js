'use strict';

const CSprite = require('./sprite');

class CBigBoom extends CSprite
{
    constructor(x, y, speed)
    {
        let frames = [
            './images/bigboom1.png',
            './images/bigboom2.png',
            './images/bigboom3.png',
            './images/bigboom4.png',
            './images/bigboom5.png'
        ];

        super(x, y, frames[0]);

        let self = this;

        this.speed = speed;
        this.complete = false;

        let textureArray = [];

        for (let i = 0; i < frames.length; i++)
        {
            let texture = PIXI.loader.resources[frames[i]].texture; // PIXI.Texture.from(frames[i]);
            textureArray.push(texture);
        }

        //this.sprite._textures = textureArray;

        this.sprite = new PIXI.extras.AnimatedSprite(textureArray);
        //this.sprite = new PIXI.extras.AnimatedSprite.fromImages(frames);

        this.sprite.width = 160;
        this.sprite.height = 65;
        this.sprite.animationSpeed = 0.2;
        this.sprite.loop = false;
        this.sprite.play();
        this.sprite.x = x;
        this.sprite.y = y;
        this.sprite.zIndex = 150;

        this.sprite.onComplete = function() {
            self.complete = true;
        }
    }

    move()
    {
        this.sprite.x -= this.speed;

        return !this.complete;
    }
}

module.exports = CBigBoom;