'use strict';

const CSprite = require('./sprite');
const CConfig = require('./config');

class CFishes extends CSprite {
    constructor(x, y, image, progress) {
        super(x, y, image);
        this.progress = progress;
        this.stage = progress.stage;
        this.recreate = false;
        this.sprite.width = 125;
        this.sprite.height = 48;
        this.sprite.zIndex = 2;
        this.config = new CConfig();

        this.max_amplitude = 10;
        this.max_sin_delimiter = 20;
        this.min_sin_delimiter = 10;

        this.amplitude = Math.random() * this.max_amplitude;
        this.sin_speed = Math.random() * this.max_sin_delimiter;
    }

    move(interval)
    {
        this.sprite.x -= this.progress.global_speed * 0.5;
    }
}

module.exports = CFishes;

