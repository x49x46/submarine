'use strict';

const Intersects = require('intersects');
const {Howl} = require('howler');
const CSprite = require('./sprite');
const CConfig = require('./config');

class CCoin extends CSprite
{
    constructor(type, progress)
    {
        let config = new CConfig();

        super(config.backPosition(40), Math.random() * config.screen.height - 40, './images/coin_' + type + '.png');

        this.config = config;
        this.progress = progress;
        this.submarine = progress.submarine;
        this.flip = '1gdHgwpnHx';

        //this.sprite.shape = new Intersects.Circle(this.sprite);

        this.sprite.width = this.sprite.height = 36;

        if (type === 'silver') {
            this.cost = 10;
        }

        if (type === 'gold') {
            this.cost = 50;
        }

        this.sound = new Howl({
            src: ['./sounds/coin2.mp3'],
            volume: 0.15
        });

        this.sprite.zIndex = 10;
    }

    move()
    {
        this.sprite.x -= this.progress.global_speed * 1.2;

        if (this.sprite.y < this.config.screen.height - this.sprite.height - 10) {
            this.sprite.y += 0.1;
        }

        if (Intersects.circlePolygon(
            this.sprite.x + Math.round(this.sprite.width / 2),
            this.sprite.y + Math.round(this.sprite.height / 2),
            Math.round(this.sprite.height / 2),
            this.submarine.getPolygon())) {

            this.progress.score += this.cost;

            this.sound.play();

            return false;
        }


        if (this.sprite.x < -70) {
            return false;
        }
    }
}

module.exports = CCoin;