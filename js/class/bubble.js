'use strict';

const CSprite = require('./sprite');

class CBubble extends CSprite
{
    constructor(x, y, image, progress)
    {
        super(x, y, image);

        this.progress = progress;
        this.sprite.width = this.sprite.height = Math.random() * 7 + 3;
        this.speed_y = this.sprite.width / 10;

        this.sprite.zIndex = 40;
    }

    move()
    {
        this.sprite.x -= this.progress.global_speed;
        this.sprite.y -= this.speed_y/* + (this.progress.global_speed / 2)*/;

        if (this.sprite.x < -this.sprite.width || this.sprite.y < -this.sprite.height) {
            return false;
        }
    }
}

module.exports = CBubble;