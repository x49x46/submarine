'use strict';

const CSprite = require('./sprite');
const CConfig = require('./config');

class CBackground extends CSprite
{
    constructor(x, y, image, progress)
    {
        super(x, y, image);
        this.progress = progress;
        this.stage = progress.stage;
        this.recreate = false;
        this.sprite.width = 1280;
        this.sprite.height = 383;
        this.sprite.zIndex = 2;

        this.config = new CConfig();
    }

    move()
    {
        this.sprite.x -= this.progress.global_speed * 0.4;

        if (this.sprite.x < 0 && !this.recreate) {
            let bg = new CBackground(this.sprite.x + 1280, this.config.screen.height - 450, './images/bgbg.png', this.progress);
            this.stage.append(bg);
            this.recreate = true;
        }

        if (this.sprite.x < -1920) {
            return false;
        }
    }
}

module.exports = CBackground;