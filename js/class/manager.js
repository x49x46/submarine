'use strict';

class CManager {
    constructor() {
        this.stages = [];
    }
    append(stage) {
        this.stages.push(stage);
    }

    move(delta) {
        this.stages.forEach(function(item, i, arr) {
            if (item.active) {
                item.move(delta);
            }
        });
    }
}
module.exports = CManager;
