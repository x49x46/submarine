const $ = require('jquery');
const PIXI = require('pixi.js');
const CConfig = require('./class/config');
const CStage = require('./class/stage');
const CProgress = require('./class/progress');
const CManager = require('./class/manager');
const CPreloader = require('./class/preloader');

$(document).ready(function() {

    let config = new CConfig();
    let interval = 0;
    let app = new PIXI.Application(config.screen.width, config.screen.height, { transparent: true });
    let progress = null;
    let stage = null;

    let frames = [
        './images/submarine_1.png',
        './images/submarine_2.png',
        './images/submarine_3.png',
    ];

    let damage_frames = [
        './images/damage1.png',
        './images/damage2.png',
        './images/damage3.png',
    ];

    let double_damage_frames = [
        './images/ddamage1.png',
        './images/ddamage2.png',
        './images/ddamage3.png',
    ];

    let loader = PIXI.loader
        .add("./images/bg.png")
        .add("./images/bgbg.png")
        .add("./images/ground.png")
        .add("./images/bubble.png")
        .add("./images/mine.png")
        .add("./images/coin_gold.png")
        .add("./images/coin_silver.png")
        .add("./images/torpedo.png")
        .add("./images/ahtung.png")
        .add("./images/fish3.png")
        .add("./images/heart.png")
        .add("./images/boom1.png")
        .add("./images/boom2.png")
        .add("./images/boom3.png")
        .add("./images/boom4.png")
        .add("./images/boom5.png")
        .add("./images/bigboom1.png")
        .add("./images/bigboom2.png")
        .add("./images/bigboom3.png")
        .add("./images/bigboom4.png")
        .add("./images/bigboom5.png")
        .add(frames)
        .add(damage_frames)
        .add(double_damage_frames)
        .load(setup);

    loader.onComplete.add(() => {
        stage = new CStage(app);
        progress = new CProgress(stage);
        progress.initSubSub();

        let manager = new CManager();

        manager.append(stage);

        app.ticker.add(function(delta) {
            interval++;
            manager.move(interval);
            progress.move();
        });

        resize();

        app.start();
    });

    function setup(loader, resources) {

    }

    $('#container').append(app.view);

    $('#restart').on('click', function() {
        stage.clear();
        progress.init();
        progress.initSub();
        $('#wnd-game-over').css('display', 'none');
        $('#win-line').css('display', 'none');
        $('#restart').addClass('hidden');
    });

    $('#rulesButton').on('click', function(){
        $('#result').toggle();
        $('#rules').toggle();

        if ($('#result').css('display') == 'none') {
            $('#rulesButton').text('Результаты');
        } else {
            $('#rulesButton').text('Правила');
        }
    });

    function resize() {
        if (need_rotate) {
            let ratio = config.screen.height / config.screen.width;

            let w = 0;
            let h = 0;

            if (window.innerHeight / window.innerWidth >= ratio) {
                w = window.innerHeight;
                h = window.innerHeight * ratio;
            } else {
                w = window.innerWidth / ratio;
                h = window.innerWidth;
            }

            app.view.style.width = w + 'px';
            app.view.style.height = h + 'px';
        } else {
            let ratio = config.screen.width / config.screen.height;

            let w = 0;
            let h = 0;

            if (window.innerWidth / window.innerHeight >= ratio) {
                w = window.innerHeight * ratio;
                h = window.innerHeight;
            } else {
                w = window.innerWidth;
                h = window.innerWidth / ratio;
            }

            app.view.style.width = w + 'px';
            app.view.style.height = h + 'px';
        }

        //$('#wnd-game-over').css('marginLeft', w / 2 - 200 );
    }
    window.onresize = function(event) {
        resize();
    };
});